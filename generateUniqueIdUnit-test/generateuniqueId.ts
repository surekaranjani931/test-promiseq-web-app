export const generateUniqueId = (): string => {
    const pattern = 'xxxxxxxx-xxxx-4xxx-yxxx-xx'
    return pattern.replace(/[xy]/g, (c: string) => {
      const r: number = (Math.random() * 16) | 0
      const v: number = c === 'x' ? r : (r & 0x3) | 0x8
      return v.toString(16)
    })
  }